# Textlocal API
This module allows you to send a text message to a UK mobile number by using
Textlocal API - https://www.textlocal.com/

# Requirements
This module communicates with Textlocal API via CURL and do not need any library
files downloaded.

# Installation
To install, copy the textlocal folder in your 'sites/all/modules' directory.
Go to Administer -> modules and look for SMS -> Textlocal API and enable this
module.

# Configuration
Go to Administer -> Configuration -> System -> Text Local.
Enter valid 'Text Local API hash' and 'email address'.
Please make sure you have enough credit in your account to send text messages.

# Current maintainer
Baber Abbasi - https://www.drupal.org/user/3385074
